.. _reg:


.. |catalogicon| image:: ../../images/CatalogIcon.png
 :target: `Sample Regression Aginity Catalog Assets`_


Lesson #6: Identify Trends Using Linear Regression
**************************************************************************

It's often helpful to analyze or group customers depending on how their usage (or purchase) of your product is trending.  This can be useful for marketing purposes or to identify those likely to attrit.  You could do this by comparing their usage last week with their usage this week but often that can be misleading.  Possibly they took a vacation last week but that doesn't mean they stopped using your product.  If you look at their usage over a longer period of time then a week of zero sales is going to have minimal impact on the overall tread.

Step 1 - Understanding Regression
===================================

One approach to doing this is to use Simple Linear Regression.  Essentially what we do is to plot the usage over a period of time and then draw a best fit line through those points.  This gives us 2 things which are helpful from a marketing and reporting perspective:-

 #. The direction of the trend (whether its up or down) will tell us if usage is increasing or decreasing
 #. The slope of the trend (how steep it is) gives us an indication of how quickly it's changing.

This is best shown with an example.  The general equation is shown below.  In order to get the best fit line 2 constants are calculated:-

- :math:`\alpha` This is the intercept.  It has minimal value for us and we will not be covering it here.
- :math:`\beta` This gives us the direction and slope of the trend discussed above.

  .. math::

    y = \alpha + \beta{x}

In our example *x* is the Time Period and *y* is the Usage during that time period.

In the example below we are tracking a specific customer's usage of our product over 8 time periods.

+-------------+--------+
| Time Period | Usage  |
+=============+========+
| 1           | 79     |
+-------------+--------+
| 2           | 80     |
+-------------+--------+
| 3           | 120    |
+-------------+--------+
| 4           | 30     |
+-------------+--------+
| 5           | 110    |
+-------------+--------+
| 6           | 80     |
+-------------+--------+
| 7           | 110    |
+-------------+--------+
| 8           | 120    |
+-------------+--------+

Lets look at it graphically.

.. image:: ../../images/regression-usagediagram.png

So you can see from the diagram that the trend is up.  What the algorithm has done here is to plot the best fit line through the points we have provided and from that we can easily see the trend.  We can also see the the value of :math:`\beta` in the equation is about 4.73.  This means that usage roughly increases 4.73 on average over each time period.  If you are interested in the math behind all this you can find more details here - `Wikipedia Simple Linear Regression`_.

..  _Wikipedia Simple Linear Regression: https://en.wikipedia.org/wiki/Simple_linear_regression

Step 2 - Building it in SQL
===========================

How do we do this in SQL ?  It's actually quite simple.  Assume we have a simple table called usage with 3 columns

usage
++++++++++++++++++++

  +------------------+--------------------------------------------------+----------------------+
  | Column name      | Column Description                               | Data Type            |
  +==================+==================================================+======================+
  | customer_id      | Unique identifier of a customer                  | Integer              |
  +------------------+--------------------------------------------------+----------------------+
  | time_period      | Time Period identifier                           | Integer              |
  +------------------+--------------------------------------------------+----------------------+
  | usage_cnt        | # of times customer used product in time period  | Integer              |
  +------------------+--------------------------------------------------+----------------------+

Some sample data is shown below.  In this data customer_id 1 is the same as the example discussed above.  You can see there are 2 customers each with 8 time periods.

.. code-block:: sql
  :linenos:

        --usage data

        select 1 as customer_id, 1 as time_period, 79 as usage_cnt union all
        select 1 as customer_id, 2 as time_period, 80 as usage_cnt union all
        select 1 as customer_id, 3 as time_period, 120 as usage_cnt union all
        select 1 as customer_id, 4 as time_period, 30 as usage_cnt union all
        select 1 as customer_id, 5 as time_period, 110 as usage_cnt union all
        select 1 as customer_id, 6 as time_period, 80 as usage_cnt union all
        select 1 as customer_id, 7 as time_period, 110 as usage_cnt union all
        select 1 as customer_id, 8 as time_period, 120 as usage_cnt union all
        select 2 as customer_id, 1 as time_period, 80 as usage_cnt union all
        select 2 as customer_id, 2 as time_period, 70 as usage_cnt union all
        select 2 as customer_id, 3 as time_period, 100 as usage_cnt union all
        select 2 as customer_id, 4 as time_period, 60 as usage_cnt union all
        select 2 as customer_id, 5 as time_period, 40 as usage_cnt union all
        select 2 as customer_id, 6 as time_period, 50 as usage_cnt union all
        select 2 as customer_id, 7 as time_period, 30 as usage_cnt union all
        select 2 as customer_id, 8 as time_period, 40 as usage_cnt

The SQL used to get the slope (:math:`\beta`) and the results is shown below

.. code-block:: postgresql
  :linenos:

      with prep as (
      select customer_id,
      sum(time_period)/count(*)  as x_bar,
      sum(usage_cnt)/count(*)  as y_bar
      from usage group by customer_id
      )
      select t1.customer_id,
             round(sum((time_period - x_bar)*(usage_cnt - y_bar)) /
                   sum((time_period - x_bar)*(time_period - x_bar)), 2) as slope
      from usage t1 inner join prep t2 on t1.customer_id = t2.customer_id
      group by t1.customer_id

The SQL does some prep work to get averages for the the *x* and *y* values (*x* being the Time Period and *y* being the Usage).  Then it applies the linear regression formula to get the slope (:math:`\beta`) for each customer.  As you can see from the results below Customer 1 has a :math:`\beta` of 4.73.  The second customer has a negative :math:`\beta` which means their usage is decreasing.  Using this data you could, for example, assign each customer to a different campaign.  e.g. If a customer has a negative :math:`\beta` then they might receive a special offer to extend their subscription.  Whereas customers with positive :math:`\beta` could be the target of a cross-sell campaign to generate more revenue since they seem to enjoy using the product.

.. image:: ../../images/regression-simpleresults.png

Step 3 - Taking it to the Next Step
====================================

So far so good.  However, the above example assumes that your data is provided in a very convenient way.  In practice there are 2 scenarios that may add complication and require additional processing.

(1) **Actual time periods**.  It's unlikely that you'll be given numeric periods starting at 1. In most cases you'll be looking at specific time periods - e.g. weeks or months.  If the numeric representation of these dates creates consistent spacing between them - e.g. 201801, 201802, 201803 then you are good (there is a spacing of 1 between all these).  For the purposes of calculating :math:`\beta` we don't care if the *x* values start at 1 or 201801.   However if there is not even spacing between them e.g. 201811, 201812, **201901** then you need to make adjustments or else the slope of your line will be misleading.

(2) **Missing records**.  Depending on how your database is set up a missing time period might be automatically created with a value of zero.  However in practice it's more likely to missing. If a customer did not use your product for a given time period then there is probably not an entry for it in the database.  If this is the case then you'll need to insert default records with a usage of 0 prior to doing your regression.  If not your trend will be misleading since it will miss periods where no activity occurs.

Let's take a look at each of these in turn.

**Actual time periods**

Typically your data will be delivered with a specific time period.  In the example data below we have year_month (rather than a simple Time Period like what we had in the first example).  You can see below that we jump from 201812 to 201901.  If we use the raw year_month value in our regression calculation then the slope will be misleading.

.. code-block:: sql
  :linenos:

        --usage_month data

        select 1 as customer_id, 201811 as year_month, 79 as usage_cnt union all
        select 1 as customer_id, 201812 as year_month, 80 as usage_cnt union all
        select 1 as customer_id, 201901 as year_month, 120 as usage_cnt union all
        select 1 as customer_id, 201902 as year_month, 30 as usage_cnt union all
        select 1 as customer_id, 201903 as year_month, 110 as usage_cnt union all
        select 1 as customer_id, 201904 as year_month, 80 as usage_cnt union all
        select 1 as customer_id, 201905 as year_month, 110 as usage_cnt union all
        select 1 as customer_id, 201906 as year_month, 120 as usage_cnt union all
        select 2 as customer_id, 201811 as year_month, 80 as usage_cnt union all
        select 2 as customer_id, 201812 as year_month, 70 as usage_cnt union all
        select 2 as customer_id, 201901 as year_month, 100 as usage_cnt union all
        select 2 as customer_id, 201902 as year_month, 60 as usage_cnt union all
        select 2 as customer_id, 201903 as year_month, 40 as usage_cnt union all
        select 2 as customer_id, 201904 as year_month, 50 as usage_cnt union all
        select 2 as customer_id, 201905 as year_month, 30 as usage_cnt union all
        select 2 as customer_id, 201906 as year_month, 40 as usage_cnt

Ideally we want to convert our year_month to a sequential time period like what we had in the first example.  The easiest way to do that is to change your Date dimension so that sequential integers are assigned to months when building the Date dimension.  Then you simply use that integer field instead of the year_month field.  Assume your date dimensions looks something like this (grossly simplified I know :) . You will note that the time_period field does not begin with 1.  That is to show that it does not need to.  As long as there is an equal interval of 1 between each  period then it doesn't matter where I start.  We will end up with the same result for :math:`\beta` .

.. code-block:: sql
  :linenos:

        --date_dim data

        select 201811 as year_month, 100 as time_period union all
        select 201812 as year_month, 101 as time_period union all
        select 201901 as year_month, 102 as time_period union all
        select 201902 as year_month, 103 as time_period union all
        select 201903 as year_month, 104 as time_period union all
        select 201904 as year_month, 105 as time_period union all
        select 201905 as year_month, 106 as time_period union all
        select 201906 as year_month, 107

The following SQL is almost identical to the that shown earlier.  It simply has extra joins to the date dimension on year_month to return the sequential time_period.  This will give the same result as the prior query.

.. code-block:: postgresql
  :linenos:

      with prep as
      (   select customer_id,
          sum(time_period)/count(*)  as x_bar,
          sum(usage_cnt)/count(*)  as y_bar
          from usage_month t1 join date_dim t2 on t1.year_month = t2.year_month group by customer_id
      )
      select t1.customer_id,
             round(sum((time_period - x_bar)*(usage_cnt - y_bar)) /
                   sum((time_period - x_bar)*(time_period - x_bar)), 2) as slope
      from usage_month t1 join prep t2 on t1.customer_id = t2.customer_id
                      join date_dim t3 on t1.year_month = t3.year_month
      group by t1.customer_id

**Missing records**

In the dataset below some data is missing.  e.g. for Customer 1 we are missing data for February (201902) and March (201903).  This means they had no usage in these months so for the purposes of calculating the regression we want to assume that usage in these months was zero.

.. code-block:: sql
  :linenos:

  --usage_month_with_gaps data

  select 1 as customer_id, 201811 as year_month, 79 as usage_cnt union all
  select 1 as customer_id, 201812 as year_month, 80 as usage_cnt union all
  select 1 as customer_id, 201901 as year_month, 120 as usage_cnt union all
  select 1 as customer_id, 201904 as year_month, 80 as usage_cnt union all
  select 1 as customer_id, 201905 as year_month, 110 as usage_cnt union all
  select 1 as customer_id, 201906 as year_month, 120 as usage_cnt union all
  select 2 as customer_id, 201811 as year_month, 80 as usage_cnt union all
  select 2 as customer_id, 201812 as year_month, 70 as usage_cnt union all
  select 2 as customer_id, 201901 as year_month, 100 as usage_cnt union all
  select 2 as customer_id, 201902 as year_month, 60 as usage_cnt union all
  select 2 as customer_id, 201905 as year_month, 30 as usage_cnt union all
  select 2 as customer_id, 201906 as year_month, 40 as usage_cnt

To do this we add yet another step to the SQL.  This time we look at all the possible combinations of customer / year_month.  Then we create an entry for customer / year_month combinations that are not already in the usage_month_with_gaps table and give those new records a usage_cnt = 0.  In the example I'm doing a cross join of all my distinct customers with all the possible year_month values from my date dimension.  In practice you'd want to filter this as much as possible for performance reasons.

.. code-block:: postgresql
  :linenos:

      with usage_prep as
      (   select * from usage_month_with_gaps union all
          select customer_id, year_month, 0 from
          (select customer_id from usage_month_with_gaps t1 group by customer_id) t1
            cross join (select year_month from date_dim) t2
          where not exists ( select 1 from usage_month_with_gaps t3
          where t1.customer_id = t3.customer_id
          and t2.year_month = t3.year_month ) order by customer_id, year_month
      ),
      prep as
      (   select customer_id,
          sum(time_period)/count(*)  as x_bar,
          sum(usage_cnt)/count(*)  as y_bar
          from usage_prep t1 join date_dim t2 on t1.year_month = t2.year_month
          group by customer_id
      )
      select t1.customer_id,
            round(sum((time_period - x_bar)*(usage_cnt - y_bar)) /
                  sum((time_period - x_bar)*(time_period - x_bar)), 2) as slope
      from usage_prep t1   join prep t2 on t1.customer_id = t2.customer_id
                           join date_dim t3 on t1.year_month = t3.year_month
      group by t1.customer_id;

so the overall trend has not changed but the values of :math:`\beta` have changed to account for the zero usage periods.

.. image:: ../../images/regression-simpleresults2.png

Other things to consider
=========================

- Don't choose too many time periods.  It obviously depends on your business but recency is typically very important.  Do you really care what your customers did a year ago ?  You may want to consider a long-term trend (using the regression method we've defined here) in combination with a short-term trend which just uses the last couple of time periods

- In addition to segmenting and marketing based on the direction of the trend also consider the slope - the actual value of :math:`\beta`.  You may want to consider differentiating between customers with slow growth in usage and those with quicker growth - similarly where product usage is declining. Remember the :math:`\beta` number is the average increase or decrease over the period being analyzed.



Sample Regression Aginity Catalog Assets
=========================================

There are eleven assets you can add to your catalog.  I chose to add them as shown below.

.. image:: ../../images/RegressionCatalog.png

These queries are written using ANSII standard SQL so should work across most database platforms.  Just select a connection in the Pro/Team Editor and either double click the catalog item and execute or drag and drop the catalog item which will expose the code and run them.

DATA-date_dim (alias = t3)
+++++++++++++++++++++++++++

This asset is a data set built using union all to represent a typical date dimension.  Note in the catalog I add the alias t3.  You can choose to do this several ways but this can be handy when using in a **Relationship** catalog item.

  .. code-block:: postgresql
    :linenos:

    (select 201811 as year_month, 100 as time_period union all
        select 201812 as year_month, 101 as time_period union all
        select 201901 as year_month, 102 as time_period union all
        select 201902 as year_month, 103 as time_period union all
        select 201903 as year_month, 104 as time_period union all
        select 201904 as year_month, 105 as time_period union all
        select 201905 as year_month, 106 as time_period union all
        select 201906 as year_month, 107
        ) t3

DATA-usage
++++++++++++++

This asset is a data asset that shows a customer, a time period and some "activity" we are labeling usage which we will trend using the regression equation.

  .. code-block:: postgresql
    :linenos:

      (
        select 1 as customer_id, 1 as time_period, 79.0 as usage_cnt union all
              select 1 as customer_id, 2 as time_period, 80.0 as usage_cnt union all
              select 1 as customer_id, 3 as time_period, 120.0 as usage_cnt union all
              select 1 as customer_id, 4 as time_period, 30.0 as usage_cnt union all
              select 1 as customer_id, 5 as time_period, 110.0 as usage_cnt union all
              select 1 as customer_id, 6 as time_period, 80.0 as usage_cnt union all
              select 1 as customer_id, 7 as time_period, 110.0 as usage_cnt union all
              select 1 as customer_id, 8 as time_period, 120.0 as usage_cnt union all
              select 2 as customer_id, 1 as time_period, 80.0 as usage_cnt union all
              select 2 as customer_id, 2 as time_period, 70.0 as usage_cnt union all
              select 2 as customer_id, 3 as time_period, 100.0 as usage_cnt union all
              select 2 as customer_id, 4 as time_period, 60.0 as usage_cnt union all
              select 2 as customer_id, 5 as time_period, 40.0 as usage_cnt union all
              select 2 as customer_id, 6 as time_period, 50.0 as usage_cnt union all
              select 2 as customer_id, 7 as time_period, 30.0 as usage_cnt union all
              select 2 as customer_id, 8 as time_period, 40.0 as usage_cnt
      )

DATA-usage_month (alias = t1)
+++++++++++++++++++++++++++++++

This asset is a data asset that shows a customer, a monthly time period and some "activity" we are labeling usage which we will trend using the regression equation.  Again we alias this catalog item with "t1".

  .. code-block:: postgresql
    :linenos:

    (select 1 as customer_id, 201811 as year_month, 79.0 as usage_cnt union all
      select 1 as customer_id, 201812 as year_month, 80.0 as usage_cnt union all
      select 1 as customer_id, 201901 as year_month, 120.0 as usage_cnt union all
      select 1 as customer_id, 201902 as year_month, 30.0 as usage_cnt union all
      select 1 as customer_id, 201903 as year_month, 110.0 as usage_cnt union all
      select 1 as customer_id, 201904 as year_month, 80.0 as usage_cnt union all
      select 1 as customer_id, 201905 as year_month, 110.0 as usage_cnt union all
      select 1 as customer_id, 201906 as year_month, 120.0 as usage_cnt union all
      select 2 as customer_id, 201811 as year_month, 80.0 as usage_cnt union all
      select 2 as customer_id, 201812 as year_month, 70.0 as usage_cnt union all
      select 2 as customer_id, 201901 as year_month, 100.0 as usage_cnt union all
      select 2 as customer_id, 201902 as year_month, 60.0 as usage_cnt union all
      select 2 as customer_id, 201903 as year_month, 40.0 as usage_cnt union all
      select 2 as customer_id, 201904 as year_month, 50.0 as usage_cnt union all
      select 2 as customer_id, 201905 as year_month, 30.0 as usage_cnt union all
      select 2 as customer_id, 201906 as year_month, 40.0 as usage_cnt) t1

DATA-usage_month_with_gaps
+++++++++++++++++++++++++++

This asset is a data asset that shows a customer, a monthly time period not sequential and some "activity" we are labeling usage which we will trend using the regression equation.

        .. code-block:: postgresql
          :linenos:

            (
            select 1 as customer_id, 201811 as year_month, 79.0 as usage_cnt union all
            select 1 as customer_id, 201812 as year_month, 80.0 as usage_cnt union all
            select 1 as customer_id, 201901 as year_month, 120.0 as usage_cnt union all
            select 1 as customer_id, 201904 as year_month, 80.0 as usage_cnt union all
            select 1 as customer_id, 201905 as year_month, 110.0 as usage_cnt union all
            select 1 as customer_id, 201906 as year_month, 120.0 as usage_cnt union all
            select 2 as customer_id, 201811 as year_month, 80.0 as usage_cnt union all
            select 2 as customer_id, 201812 as year_month, 70.0 as usage_cnt union all
            select 2 as customer_id, 201901 as year_month, 100.0 as usage_cnt union all
            select 2 as customer_id, 201902 as year_month, 60.0 as usage_cnt union all
            select 2 as customer_id, 201905 as year_month, 30.0 as usage_cnt union all
            select 2 as customer_id, 201906 as year_month, 40.0 as usage_cnt
            )

FML - Slope
++++++++++++

This formulaic asset is used to store the equation for slope once but reuse in several other regression analysis.

    .. code-block:: postgresql
      :linenos:

      sum((time_period - x_bar)*(usage_cnt - y_bar)) /
                   sum((time_period - x_bar)*(time_period - x_bar))

FML - x-bar
++++++++++++

This formulaic asset is used to plot the x axis variable which is the time period in this case.

  .. code-block:: postgresql
    :linenos:

    sum(time_period)/count(*)

FML - y-bar
+++++++++++++


This formulaic asset is used to plot the y axis variable which is the average "activity" or usage count in this case.

  .. code-block:: postgresql
    :linenos:

    sum(usage_cnt)/count(*)

JOIN Usage Month to Date Dim
+++++++++++++++++++++++++++++

This is a relationship object that is used to store join paths once and allow you to reuse them.

  .. code-block:: postgresql
    :linenos:

    t1.year_month = t3.year_month

  .. note::

    See the usage of the alias we reference above.

Sample - Usage Query - Simple
++++++++++++++++++++++++++++++++

This query asset will use the data, formulas and joins described above to perform the regression.

    .. code-block:: postgresql
      :linenos:


        with prep as (
              select customer_id,
                    @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/FML - x-bar} as x_bar,
                    @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/FML - y-bar} as y_bar
              from @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-usage} usage
              group by
                  customer_id
              )
              select
                    t1.customer_id,
                    round(@{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/FML - Slope}, 2) as slope
              from @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-usage} t1
                inner join prep t2 on t1.customer_id = t2.customer_id
              group by
                      t1.customer_id



Sample - Usage Query - Gaps
++++++++++++++++++++++++++++++

This query asset will use the data, formulas and joins described above to fill in gaps with zero data and then perform the regression.


    .. code-block:: postgresql
      :linenos:


         with usage_prep as
              (   select * from @{Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-usage_month_with_gaps} usage union all
                  select customer_id, year_month, 0 from
                  (select customer_id from @{Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-usage_month_with_gaps} t1 group by customer_id) t1
                    cross join (select year_month from @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-date_dim (alias = t3)}) t2
                  where not exists ( select 1 from @{Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-usage_month_with_gaps} t3
                  where t1.customer_id = t3.customer_id
                  and t2.year_month = t3.year_month )
                  order by customer_id, year_month
              ),
              prep as
              (   select customer_id,
                  @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/FML - x-bar} as x_bar,
                  @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/FML - y-bar}  as y_bar
                  from usage_prep t1 join @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-date_dim (alias = t3)} on t1.year_month = t3.year_month
                  group by customer_id
              )
              select t1.customer_id,
                    round(@{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/FML - Slope}, 2) as slope
              from usage_prep t1   join prep t2 on t1.customer_id = t2.customer_id
                                   join @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-date_dim (alias = t3)} on t1.year_month = t3.year_month
              group by t1.customer_id




Sample Usage Query - Month
+++++++++++++++++++++++++++++

This query asset will use the data, formulas and joins described above aggregate to a month and then perform the regression.


    .. code-block:: postgresql
      :linenos:


        with prep as
        (   select customer_id,
            @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/FML - x-bar}  as x_bar,
            @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/FML - y-bar}  as y_bar
            from @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-usage_month (alias = t1)}
            join @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-date_dim (alias = t3)}
              on @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/JOIN Usage Month to Date Dim}
            group by customer_id
        )
        select t1.customer_id,
               round(@{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/FML - Slope}, 2) as slope
        from @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-usage_month (alias = t1)}
        join prep t2
          on t1.customer_id = t2.customer_id
        join @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/DATA-date_dim (alias = t3)}
               on @{/Samples/Sample Data Science Queries - All Platforms/Regression Samples/JOIN Usage Month to Date Dim}
        group by t1.customer_id
